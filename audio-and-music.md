# Audio and music

## digital audio workstation (DAW)

* [Ardour](https://ardour.org)
* [LMMS](https://lmms.io)
* [MusE](https://muse-sequencer.github.io)
* [Qtractor](https://www.qtractor.org)
* [Zrythm](https://www.zrythm.org)

## plugin

* [Surge XT](https://surge-synthesizer.github.io)
* [ZynAddSubFX](https://zynaddsubfx.sourceforge.io)

## plugin host

* [Carla](https://github.com/falkTX/Carla/)
